const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    const fighters = FighterService.getList();

    res.data = fighters;

    next();
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    const id = req.params.id;
    const fighter = FighterService.search({id});

    if (fighter) {
        res.data = fighter;
    } else {
        res.err = 'Fighter not found'
    }

    next();
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {

    if (!res.err) {
        const data = FighterService.create(req.body);
        if (data) {
            res.data = data
        } else {
            res.err = 'Error while pocessing data, try again later';
        }
    }

    next();
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    const id = req.params.id;

    if (!res.err) {
        const result = FighterService.update(id, req.body);
        if (result && result.id) {
            res.data = result;
        } else {
            res.err = 'Something went wrong, check fighter id and try again later';
        }
    }

    next();
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    const id = req.params.id;
    const data = FighterService.delete(id);

    if (data && data.length) {
        res.data = data;
    } else {
        res.err = 'Fighter with such id not found'
    }

    next();
}, responseMiddleware)

module.exports = router;