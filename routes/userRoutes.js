const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    res.data = UserService.getList();

    next();
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    const id = req.params.id;
    const user = UserService.search({id});
    if (user) {
        res.data = user;
    } else {
        res.err = 'User not found';
    }

    next();
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {

    if (!res.err) {
        const result = UserService.create(req.body)
        if (result) {
            res.data = result;
        } else {
            res.err = 'Error while pocessing data, try again later';
        }
    }

    next();
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {

    if (!res.err) {
        const result = UserService.update(req.params.id, req.body);
        if (result && result.id) {
            res.data = result;
        } else {
            res.err = 'Something went wrong, check user id and try again later';
        }
    }

    next();
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    const id = req.params.id;
    const data = UserService.delete(id);

    if (data && data.length) {
        res.data = data;
    } else {
        res.err = 'No user with such id';
    }

    next();
}, responseMiddleware);

module.exports = router;