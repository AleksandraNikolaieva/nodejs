const { BaseRepository } = require('./baseRepository');
const { getFilteredData } = require('../common/helpers');
const { user } = require('../models/user');

class UserRepository extends BaseRepository {
    constructor() {
        super('users');
    }

    update(id, dataToUpdate) {
        const filteredData = getFilteredData(dataToUpdate, user);

        return super.update(id, filteredData);
    }

    create(data) {
        const filteredData = getFilteredData(data, user);

        return super.create(filteredData);
    }
}

exports.UserRepository = new UserRepository();