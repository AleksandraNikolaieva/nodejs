const { BaseRepository } = require('./baseRepository');
const { getFilteredData } = require('../common/helpers');
const { fighter } = require('../models/fighter');


class FighterRepository extends BaseRepository {
    constructor() {
        super('fighters');
    }

    update(id, dataToUpdate) {
        const filteredData = getFilteredData(dataToUpdate, fighter);

        return super.update(id, filteredData);
    }

    create(data) {
        const filteredData = getFilteredData(data, fighter);

        return super.create(filteredData);
    }
}

exports.FighterRepository = new FighterRepository();