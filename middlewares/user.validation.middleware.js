const { user } = require('../models/user');
const { getEmptyFieldErrors } = require('../common/helpers');

const createUserValid = (req, res, next) => {
    const userData = req.body;
    const errors = getUserValidationErrors(userData);

    if (errors.length)   {
        res.err = errors;
    }

    next();
}

const updateUserValid = (req, res, next) => {
    const userData = req.body;
    const errors = getUserValidationErrors(userData);

    if (errors.length) {
        res.err = errors;
    }

    next();
}

function getUserValidationErrors(userData) {
    if (!userData) {
        return ['No provided user data'];
    }

    const errors = getEmptyFieldErrors(userData, user);
    const { email, phoneNumber, password, id } = userData;
    
    if (email && !/^[\w.+\-]+@gmail\.com$/.test(email)) {
        errors.push('Email is invalid');
    }

    if (phoneNumber && !/^\+380\d{9}$/.test(phoneNumber)) {
        errors.push('Phone number is invalid');
    }

    if (password && password.length < 3) {
        errors.push('Password should be at least 3 characters');
    }

    if (id) {
        errors.push('Id should not be provided');
    }

    return errors;
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;