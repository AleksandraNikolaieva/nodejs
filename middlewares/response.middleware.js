const responseMiddleware = (req, res, next) => {
    const error = res.err;
    if (error) {
        let message;
        if (Array.isArray(error)) {
            message = '';

            error.forEach((errorItem, i) => {
                message += i ? `, ${errorItem}` : errorItem;
            })
        } else {
            message = error;
        }

        res.status(400).json({error: true, message});
    } else {
        res.status(200).json(res.data);
    }
    next();
}

exports.responseMiddleware = responseMiddleware;