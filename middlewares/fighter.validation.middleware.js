const { fighter } = require('../models/fighter');
const { getEmptyFieldErrors } = require('../common/helpers');

const createFighterValid = (req, res, next) => {
    const errors = getFighterValidationErrors(req.body);

    if (errors.length) {
        res.err = errors
    }

    next();
}

const updateFighterValid = (req, res, next) => {
    const errors = getFighterValidationErrors(req.body);

    if (errors.length) {
        res.err = errors
    }

    next();
}

function getFighterValidationErrors(fighterData) {

    if (!fighterData) {
        return ['No fighter data provided'];
    }

    const errors = getEmptyFieldErrors(fighterData, fighter);
    const { power, defense } = fighterData;
    
    if (power) {
        if(isNaN(power)) {
            errors.push('Power is not a number');
        } else if (power > 100) {
            errors.push('Power should be less then 100');
        }
    }

    if (defense) {
        if (isNaN(defense)) {
            errors.push('Defense is not a number');
        } else if (defense < 1) {
            errors.push('Defense should be more then 1');
        } else if (defense > 10) {
            errors.push('Defense should be less then 10');
        }
    }

    return errors;
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;