const getEmptyFieldErrors = (data, model) => {
    const emptyFieldErrors = [];

    Object.keys(model).map(key => {
        if (key !== 'id') {

            if (!data[key] && !model[key]) {
                emptyFieldErrors.push(`${key} is required`);
            }
        }
    });

    return emptyFieldErrors;
}

const getFilteredData = (data, model) => {
    const filteredData = {};

    Object.keys(model).forEach(key => {
        if (key !== 'id') {
            filteredData[key] = data[key] || model[key];
        }
    })

    return filteredData;
}

exports.getEmptyFieldErrors = getEmptyFieldErrors;
exports.getFilteredData = getFilteredData;
