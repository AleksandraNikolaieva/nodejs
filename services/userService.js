const { UserRepository } = require('../repositories/userRepository');

class UserService {

    getList() {
        return UserRepository.getAll();
    }

    search(search) {
        return search ? UserRepository.getOne(search) : null;
    }

    create(user) {
        return user ? UserRepository.create(user) : null;
    }

    update(id, updates) {
        return id && updates ? UserRepository.update(id, updates) : null;
    }

    delete(id) {
        return id ? UserRepository.delete(id) : null;
    }
}

module.exports = new UserService();