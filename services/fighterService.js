const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {

    getList() {
        return FighterRepository.getAll();
    }

    search(search) {
        return search ? FighterRepository.getOne(search) : null;
    }

    create(fighter) {
        return fighter ? FighterRepository.create(fighter) : null;
    }

    update(id, updates) {
        return id && updates ? FighterRepository.update(id, updates) : null;
    }

    delete(id) {
        return id ? FighterRepository.delete(id) : null;
    }
}

module.exports = new FighterService();